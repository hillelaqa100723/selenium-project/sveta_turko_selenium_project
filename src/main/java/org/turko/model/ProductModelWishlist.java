package org.turko.model;

import lombok.Data;
import lombok.experimental.Accessors;
@Data
@Accessors(fluent = true)
public class ProductModelWishlist {
    public String productName;
    public Double price;
    public int qty;
}




