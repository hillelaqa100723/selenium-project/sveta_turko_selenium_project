package org.turko.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;


@Data
@Accessors(fluent = true)
public class ProductModelCart extends ProductModel {
    public String productName;
    public Double price;
    public int qty;
}

