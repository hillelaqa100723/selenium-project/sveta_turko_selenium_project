package org.turko.pages.content.GiftCard;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.turko.driver.WebDriverHolder;
import org.turko.pages.content.BaseContentPage;

public class GiftCardsPage extends BaseContentPage {

    public void clickProductByName() {
        By productLocator = By.linkText("$50 Physical Gift Card");
        WebElement product = findElement(productLocator);
        product.click();
    }
    private WebElement findElement(By locator) {
        return WebDriverHolder.getInstance().getDriver().findElement(locator);
    }
}

