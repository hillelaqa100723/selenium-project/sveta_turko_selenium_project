package org.turko.pages.content.GiftCard;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.turko.pages.base.BasePage;


public class ProductGiftCardsPage extends BasePage {

    @FindBy(xpath = "//div/button[contains(text(),'Add to cart') and @id]")
    public static WebElement addToCardButton;

    @FindBy(xpath = "//div[@id='bar-notification']//p")
    public WebElement errorMessageText;

    @FindBy(xpath = "//div[@id='bar-notification']")
    public static WebElement errorMessage;

    @FindBy(xpath = "//input[@class='recipient-name']")
    public static WebElement RecipientNameField;

    @FindBy(xpath = "//input[@class='sender-name']")
    public static WebElement SenderNameField;

    public static boolean isErrorMessage() {
        return errorMessage.isDisplayed();
    }

    public static void clickOnAddToCartButton() {
        addToCardButton.click();
    }

    public String getErrorMessage() {
        return errorMessageText.getText().trim();
    }
    public static void enterRecipientName(String recipientName) {
        RecipientNameField.clear();
        RecipientNameField.sendKeys(recipientName);
    }

    public static void enterSenderNameField(String SenderName) {
        SenderNameField.clear();
        SenderNameField.sendKeys(SenderName);
    }
}
