package org.turko.pages.content.computers;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.turko.pages.base.BasePage;

public class ProductNotebooksPage extends BasePage {

    @FindBy(xpath = "//div/button[contains(text(),'Add to cart') and @id]")
    public WebElement addToCardButton;
    @FindBy(xpath = "//div[contains(@class,'bar-notification success')]")
    private WebElement infoMessage;
    @FindBy(xpath = "//strong[contains(text(),'Related products')]")
    private WebElement pageTitle;


    public boolean isPageWithItemLoaded() {
        return pageTitle.isDisplayed();
    }

    public void clickOnAddToCartButton() {
        addToCardButton.click();
    }

    public boolean isInfoMessageDisplayed() {
        return infoMessage.isDisplayed();
    }
}

