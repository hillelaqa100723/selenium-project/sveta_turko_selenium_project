package org.turko.pages.content.computers;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.turko.driver.WebDriverHolder;
import org.turko.model.ProductModel;
import org.turko.pages.compare.CompareProductsListPage;
import org.turko.pages.content.BaseContentPage;

import java.util.List;

@Slf4j
public class NotebooksPage extends BaseContentPage {

    public List<ProductModel> getNotebooks() {
        return getProductAsList();
    }

    @FindBy(xpath = "//a[contains(text(),'Next')]")
    private static WebElement nextPageButton;

    @FindBy(xpath = "//a[contains(text(),'Previous')]")
    private static WebElement previousPageButton;

    @FindBy(xpath = "//li[@class='current-page']")
    private static WebElement highlightedPageNumber;

    public ProductModel findProductByName(String productName) {
        List<ProductModel> notebooks = getNotebooks();

        for (ProductModel product : notebooks) {
            if (product.productName().equals(productName)) {
                return product;
            }
        }
        return null;
    }

    public NotebooksPage addNotebookToCart(String noteBookName) {
        addItemToCard(noteBookName);
        return new NotebooksPage();
    }

    public NotebooksPage addNotebookToCompare(String noteBookName) {
        addItemToCompare(noteBookName);
        sleep(2000);
        return new NotebooksPage();
    }

    public NotebooksPage addNotebooksToCompare(List<String> noteBookNames) {
        for (String noteBookName : noteBookNames) {
            addNotebookToCompare(noteBookName);
        }
        return new NotebooksPage();
    }

    public void clickProductByName(String productName) {
        By productLocator = By.linkText("HP Spectre XT Pro UltraBook");
        WebElement product = findElementWithLocator(productLocator);
        product.click();
    }

    private WebElement findElementWithLocator(By locator) {
        return WebDriverHolder.getInstance().getDriver().findElement(locator);
    }

    public static boolean areActualCurrencyPrice(String currencyCode) {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();

        List<WebElement> productPrices = driver.findElements(By.xpath("//span[@class='price actual-price']"));

        for (WebElement productPrice : productPrices) {
            if (!productPrice.getText().contains(currencyCode)) {
            }
        }
        return true;
    }

    public NotebooksPage addNoteBookToWishlist(String noteBookName) {
        addItemToWishlist(noteBookName);
        return new NotebooksPage();
    }

    public void clickOnNextPageButton() {
        nextPageButton.click();
    }

    public void clickOnPrevPageButton() {
        previousPageButton.click();
    }

    public int getHighlightedPageNumber() {
        String pageNumberText = highlightedPageNumber.getText();
        return Integer.parseInt(pageNumberText);
    }

    public boolean isProductQtyTrue(int expQty) {
        log.info("Checking if product quantity is true: {}", expQty);
        return getProductQty() == expQty;
    }

    public CompareProductsListPage goToCompareList() {
        WebDriverHolder.getInstance()
                .getDriver()
                .findElement(By.cssSelector(".customer-service .list"))
                .findElement(By.xpath(".//a[text()='Compare products list']"))
                .click();
        return new CompareProductsListPage();
    }

}