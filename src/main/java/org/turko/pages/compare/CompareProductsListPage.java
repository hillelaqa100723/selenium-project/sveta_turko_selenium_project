package org.turko.pages.compare;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.turko.driver.WebDriverHolder;
import org.turko.model.ProductModel;
import org.turko.pages.base.BasePage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
public class CompareProductsListPage extends BasePage {

    @FindBy(css = ".compare-products-table")
    private WebElement compareTable;

    public List<ProductModel> getProductsInfo() {
        sleep(2000);
        List<WebElement> productNamesWebElements = compareTable.findElements(By.cssSelector("tr.product-name a"));
        List<WebElement> productPricesWebElements = compareTable.findElements(By.cssSelector("tr.product-price td"));
        productPricesWebElements = productPricesWebElements.subList(1, productPricesWebElements.size());

        List<ProductModel> list = new ArrayList<>();

        for (int i = 0; i < productNamesWebElements.size(); i++) {
            ProductModel productModel = new ProductModel();
            productModel.productName(productNamesWebElements.get(i).getText().trim());
            String productPriceAsString = productPricesWebElements.get(i).getText();
            Double price = Double.parseDouble(productPriceAsString.substring(1).replaceAll(",", ""));
            productModel.price(price);
            list.add(productModel);
        }
        return list;
    }

    public CompareProductsListPage clearList() {
        WebDriverHolder.getInstance().getDriver()
                .findElement(By.cssSelector(".clear-list"))
                .click();
        return new CompareProductsListPage();
    }

    @FindBy(css = ".no-data")
    public WebElement noDataMessage;

    @FindBy(xpath = "//tbody/tr[@class='product-price']/td[@style]")
    private static List<WebElement> compareListItems;

    public List<String> getCompareProductPrices() {
        log.info("Getting product prices from the comparison page.");
        return compareListItems.stream()
                .map(this::extractNumericValueFromPrice)
                .collect(Collectors.toList());
    }

    private String extractNumericValueFromPrice(WebElement item) {
        String priceText = item.getText().trim();
        double numericValue = Double.parseDouble(priceText.replaceAll("[^0-9.]", ""));
        return String.format("%.2f", numericValue);
    }

    public String getNoItemsMessage() {
        return noDataMessage.getText().trim();
    }
}

