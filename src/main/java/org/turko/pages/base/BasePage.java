package org.turko.pages.base;

import lombok.SneakyThrows;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.turko.driver.WebDriverHolder;
import org.turko.pages.base.main_menu.MainMenu;
import org.turko.pages.base.navigate_menu.NavigateMenu;
import org.turko.pages.compare.CompareProductsListPage;
import org.turko.pages.login.LoginPage;


import java.util.List;

public class BasePage {
    @FindBy(id = "customerCurrency")
    private WebElement selectCurrency;

    @FindBy(css = ".bar-notification.success")
    private WebElement successMessage;

    @FindBy(xpath = "//span[@class='cart-qty']")
    private static WebElement cartQuantity;

    @FindBy(xpath = "//span[@class='wishlist-qty']")
    private static WebElement wishlistQuantity;

    public BasePage() {
        PageFactory.initElements(WebDriverHolder.getInstance().getDriver(), this);
    }

    public static MainMenu getMainMenu() {
        return new MainMenu();
    }

    public MainMenu isRegisterMenuItemVisible() {
        return new MainMenu();
    }

    public NavigateMenu getNavigationMenu() {
        return new NavigateMenu();
    }

    public boolean isTheMainPage() {
        return BasePage.cartQuantity.isDisplayed();
    }

    public BasePage selectCurrency(Currencies currency) {
        new Select(selectCurrency).selectByVisibleText(currency.getValue());
        return new BasePage();
    }

    @SneakyThrows
    protected void sleep(long ms) {
        Thread.sleep(ms);
    }

    public BasePage waitForSuccessMessage() {
        WebDriverHolder.getInstance()
                .getWait()
                .until(ExpectedConditions.visibilityOf(successMessage));
        return this;
    }

    public BasePage closeSuccessMessage() {
        successMessage.findElement(By.cssSelector(".close")).click();
        return this;

    }

    public boolean isTheLoginPageLoaded() {
        return LoginPage.getMainMenu().isMyAccountMenuItemVisible();
    }

    public static boolean verifyAllProductPricesInCurrency(String currencyCode) {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();

        List<WebElement> productPrices = driver.findElements(By.xpath("//span[@class='price actual-price']"));

        if (productPrices.isEmpty()) {
            System.out.println("No product prices found on the page.");
            return false;
        }

        for (WebElement productPrice : productPrices) {
            if (!productPrice.getText().contains(currencyCode)) {
                System.out.println("Product price is not in the expected currency: " + productPrice.getText());
                return false;
            }
        }
        return true;
    }

    public static int getShoppingCartValue() {
        String cartValueText = cartQuantity.getText();
        String numericPart = cartValueText.replaceAll("\\D", "");

        try {
            return Integer.parseInt(numericPart);
        } catch (NumberFormatException e) {
            return +1;
        }
    }
    public static int getWishlistValue() {
        String wishlistValueText = wishlistQuantity.getText();
        String numericPart = wishlistValueText.replaceAll("\\D", "");

        try {
            return Integer.parseInt(numericPart);
        } catch (NumberFormatException e) {
            return +1;
        }
    }

}