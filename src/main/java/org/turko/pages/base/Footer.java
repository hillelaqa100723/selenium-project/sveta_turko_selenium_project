package org.turko.pages.base;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.turko.pages.base.BasePage;


public class Footer extends BasePage {
    @FindBy(xpath = "//a[contains(text(),'Compare products list')]")
    private WebElement CompareProductsList;

    public void clickOnCompareProductsListButton(){
        CompareProductsList.click();
    }
}
