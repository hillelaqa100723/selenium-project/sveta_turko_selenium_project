package org.turko.pages.base.cart;

import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.turko.driver.WebDriverHolder;
import org.turko.model.ProductModelCart;
import org.turko.pages.base.BasePage;

import java.util.LinkedList;
import java.util.List;

@Slf4j
public class CartPage extends BasePage {

    @FindBy(css = "table.cart")
    private WebElement itemsTable;
    @FindBy(xpath = "/html/body/div[6]/div[4]/div/div/div/div[2]/div/form/div[1]/table/tbody/tr[1]/td[5]/div/div[1]")
    private WebElement qtyUpButton;

    public List<ProductModelCart> getProductsFromTable() {
        List<ProductModelCart> list = new LinkedList<>();

        for (WebElement row : itemsTable.findElements(By.cssSelector("tbody.tr"))) {
            ProductModelCart productModelCart = new ProductModelCart();
            String name = row.findElement(By.cssSelector(".product-name")).getText().trim();
            String priceAsText = row.findElement(By.cssSelector(".product-unit-price"))
                    .getText()
                    .substring(0)
                    .replaceAll(",", "");
            String qtyAsString = row.findElement(By.cssSelector(".qty-input")).getAttribute("value");

            productModelCart.productName(name);
            productModelCart.price(Double.parseDouble(priceAsText));
            productModelCart.qty(Integer.parseInt(qtyAsString));
            list.add(productModelCart);
        }
        return list;
    }
    public ProductModelCart getProductInfo(String itemName) {
        List<ProductModelCart> productsFromTable = getProductsFromTable();
        return productsFromTable
                .stream()
                .filter(productModelCart -> productModelCart.productName().equals(itemName))
                .findFirst()
                .get();
    }
    public CartPage removeItem(String itemName) {
        for (WebElement row : itemsTable.findElements(By.cssSelector("tbody tr"))) {
            if (row.findElement(By.cssSelector(".product-name")).getText().equals(itemName)) {
                row.findElement(By.cssSelector(".remove-btn")).click();
                return new CartPage();
            }
        }
        return new CartPage();
    }
    public double calcTotalProductPrice(String productName) {
        List<ProductModelCart> productsFromTable = getProductsFromTable();
        double totalAmount = productsFromTable.stream()
                .filter(productModelCart -> productModelCart.productName().equals(productName))
                .mapToDouble(ProductModelCart::price)
                .sum();
        log.info("Total product cost calculated {}: {}", productName, totalAmount);
        return totalAmount;
    }
    public static double getTotalAmountFromCart() {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();
        WebElement totalAmount = driver.findElement(By.xpath("//td/span[@class='value-summary']/strong"));
        String totalAmountText = totalAmount.getText();
        double totalAmountValue = Double.parseDouble(totalAmountText.replaceAll("[^\\d.]+", ""));
        log.info("Received the total value of the cart: {}", totalAmountValue);
        return totalAmountValue;
    }
    public void clickOnQtyUpButton() {
        qtyUpButton.click();
    }
    public CartPage agreeWithTheTermsOfService() {
        WebElement checkbox = WebDriverHolder.getInstance().getDriver().findElement(By.id("termsofservice"));
        if (!checkbox.isSelected()) {
            checkbox.click();
        }
        return this;
    }

}

