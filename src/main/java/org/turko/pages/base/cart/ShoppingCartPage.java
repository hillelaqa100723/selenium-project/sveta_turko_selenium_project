package org.turko.pages.base.cart;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.turko.driver.WebDriverHolder;
import org.turko.pages.base.BasePage;

import java.util.List;

public class ShoppingCartPage extends BasePage{
    @FindBy(xpath = "//table[@class='cart']//tr/td[@class='quantity']")
    private static List<WebElement> productRows;

    public void deleteItemFromCart() {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();

        By deleteButtonLocator = By.xpath("//button[@class='remove-btn']");
        java.util.List<WebElement> deleteButtons = driver.findElements(deleteButtonLocator);
        if (!deleteButtons.isEmpty()) {
            deleteButtons.get(0).click();
        } else {
            System.out.println("No items to delete in the cart.");
        }
    }

    public int getNumberOfProducts() {
        WebDriver driver = WebDriverHolder.getInstance().getDriver();
        By productNumberLocator = By.xpath("//td[@class='product']");
        java.util.List<WebElement> productElements = driver.findElements(productNumberLocator);
        return productElements.size();
    }
}

