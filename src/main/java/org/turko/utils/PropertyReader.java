package org.turko.utils;

import java.io.IOException;
import java.util.Properties;

public class PropertyReader {
    private static PropertyReader instance = null;
    private Properties properties;

    private PropertyReader() {
        try {
            String appName = System.getProperty("appName", "app");
            properties = new Properties();
            properties.load(getClass().getClassLoader().getResource("app.properties").openStream());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public synchronized static PropertyReader getInstance() {
        if (instance == null) {
            instance = new PropertyReader();
        }
        return instance;
    }

    public String getProperty(String propertyName){
        return properties.getProperty(propertyName);
    }

}