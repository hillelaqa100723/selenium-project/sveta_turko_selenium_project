package org.turko;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.turko.model.ProductModel;
import org.turko.model.SortByDirection;
import org.turko.pages.base.BasePage;
import org.turko.pages.compare.CompareProductsListPage;
import org.turko.pages.content.computers.NotebooksPage;
import org.turko.pages.base.Footer;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CompareProductsTest extends BaseTest {

    @BeforeMethod
    public void beforeClass() {
        openUrl();
        new BasePage()
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser();
    }

    @Test
    public void addNoteBooksToCompare() throws InterruptedException {
        new BasePage()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers", "Notebooks");

        List<String> noteBooksToCompare = List.of("HP Envy 15.6-Inch Sleekbook",
                "Asus Laptop",
                "Lenovo Thinkpad Carbon Laptop");

        List<ProductModel> productsInfo = new NotebooksPage()
                .addNotebooksToCompare(noteBooksToCompare)
                .getNotebooks();

        List<ProductModel> expectedProducts = new ArrayList<>();

        for (String name : noteBooksToCompare) {
            for (int i = 0; i < productsInfo.size(); i++) {
                if (productsInfo.get(i).productName().equals(name)) {
                    expectedProducts.add(productsInfo.get(i));
                    break;
                }
            }
        }
        List<ProductModel> compared = new NotebooksPage()
                .goToCompareList()
                .getProductsInfo();

        compared.sort(ProductModel.getComparator(SortByDirection.NAME_A_Z));
        expectedProducts.sort(ProductModel.getComparator(SortByDirection.NAME_A_Z));

        assertThat(compared)
                .asList()
                .isEqualTo(expectedProducts);

        new Footer().clickOnCompareProductsListButton();
        Thread.sleep(2000);

        CompareProductsListPage compareProductsListPage= new CompareProductsListPage();

        List<String> expectedPrices = List.of("1460.00", "1500.00", "1360.00");
        List<String> actualPrices = new CompareProductsListPage().getCompareProductPrices();

        for (String expectedPrice : expectedPrices) {
            System.out.println("Expected Price: " + expectedPrice);
        }

        new CompareProductsListPage().clearList();
        Thread.sleep(2000);

        assertThat(compareProductsListPage.getNoItemsMessage())
                .as("No-data content text should be visible")
                .isEqualTo("You have no items to compare.");
    }
}

