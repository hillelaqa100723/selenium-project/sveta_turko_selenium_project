package org.turko;

import org.turko.pages.*;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.turko.pages.base.BasePage;
import org.turko.pages.content.computers.NotebooksPage;

import static org.assertj.core.api.Assertions.assertThat;

public class DeleteItemFromCartTest extends BaseTest {

    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void deleteProduct() throws InterruptedException {
        new BasePage()
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers",
                        "Notebooks");

        int initialCartValue = BasePage.getShoppingCartValue();

        new NotebooksPage().addNotebookToCart("HP Spectre XT Pro UltraBook");

        Thread.sleep(6000);

        new BasePage();
        BasePage.getMainMenu().selectShoppingCart();

        int initialNumberOfProducts = new ShoppingCartPage().getNumberOfProducts();

        int updatedCartValue = new BasePage().getShoppingCartValue();
        assertThat(updatedCartValue)
                .as("Number of products on Shopping cart should be correct")
                .isEqualTo(initialCartValue + 1);

        new ShoppingCartPage().deleteItemFromCart();
        new ShoppingCartPage().getNumberOfProducts();

        int updatedNumberOfProducts = new ShoppingCartPage().getNumberOfProducts();
        assertThat(updatedNumberOfProducts)
                .as("Number of products in Shopping card should be correct")
                .isEqualTo(initialNumberOfProducts - 1);
    }

}