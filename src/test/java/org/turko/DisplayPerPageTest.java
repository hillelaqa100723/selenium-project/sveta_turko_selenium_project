package org.turko;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.turko.pages.base.BasePage;
import org.turko.pages.content.computers.NotebooksPage;

import static org.assertj.core.api.Assertions.assertThat;

public class DisplayPerPageTest extends BaseTest {

    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void DisplayPerPage() throws InterruptedException {
        openUrl();
        BasePage
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers",
                        "Notebooks");

        new NotebooksPage().selectDisplayPerPage(3);
        Thread.sleep(1000);

        assertThat(new NotebooksPage().isProductQtyTrue(3))
                .as("The number of notebooks on page should display correctly - 3 products")
                .isTrue();

        new NotebooksPage().selectDisplayPerPage(6);
        Thread.sleep(1000);

        assertThat(new NotebooksPage().isProductQtyTrue(6))
                .as("The number of notebooks on page should display correctly - 6 products")
                .isTrue();
    }
}