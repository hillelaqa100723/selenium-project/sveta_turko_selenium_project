package org.turko;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.turko.driver.WebDriverHolder;
import org.turko.pages.base.BasePage;
import org.turko.pages.base.cart.CartPage;
import org.turko.pages.content.computers.NotebooksPage;

import static org.assertj.core.api.Assertions.assertThat;

public class ChangeNumberOfItemsInCartTest extends  BaseTest {
    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void changeNumberOfItemsInCart() throws InterruptedException {
        new NotebooksPage();

        BasePage.getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers", "Notebooks");

        String note1 = "Asus Laptop";
        String note2 = "HP Envy 15.6-Inch Sleekbook";

        new NotebooksPage()
                .addNotebookToCart(note1)
                .addNotebookToCart(note2);

        Thread.sleep(2000);

        JavascriptExecutor js = (JavascriptExecutor) WebDriverHolder.getInstance().getDriver();
        js.executeScript("window.scrollTo(0, 0)");

        CartPage cartPage = BasePage
                .getMainMenu()
                .selectShoppingCart();

        int actualCartQuantity = cartPage.getShoppingCartValue();
        new CartPage().clickOnQtyUpButton();

        int exCartQuantity = cartPage.getShoppingCartValue();
        assertThat(exCartQuantity)
                .as("Number of products on Shopping cart should be correct")
                .isEqualTo(actualCartQuantity + 1);
    }
}


