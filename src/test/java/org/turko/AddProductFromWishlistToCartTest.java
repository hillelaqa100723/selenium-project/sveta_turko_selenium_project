package org.turko;
import org.openqa.selenium.JavascriptExecutor;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.turko.driver.WebDriverHolder;
import org.turko.model.ProductModelWishlist;
import org.turko.pages.base.BasePage;
import org.turko.pages.WishlistPage;
import org.turko.pages.base.cart.CartPage;
import org.turko.pages.base.navigate_menu.NavigateMenu;
import org.turko.pages.content.computers.NotebooksPage;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class AddProductFromWishlistToCartTest extends BaseTest {
    @BeforeMethod
    public void beforeMethod() {
        WebDriverHolder.getInstance().getDriver().manage().window().maximize();
        openUrl();
        BasePage
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser();
    }

    @Test
    public void AddProductFromWishlistToCart() throws InterruptedException {

        new BasePage()

                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers", "Notebooks");

        String note1 = "Asus Laptop";
        String note2 = "HP Envy 15.6-Inch Sleekbook";
        String note3 = "HP Spectre XT Pro UltraBook";
        String note4 = "Lenovo Thinkpad Carbon Laptop";
        String note5 = "Samsung Premium Ultrabook";

        int expectedCartQuantity = 2;

        new NotebooksPage()
                .addNotebookToCart(note1)
                .addNotebookToCart(note2);
        Thread.sleep(2000);
        int actualCartQuantity = new WishlistPage().getShoppingCartValue();

        assertThat(actualCartQuantity)
                .as("The actual number of items in the cart should match the expected quantity")
                .isEqualTo(expectedCartQuantity);

        new NotebooksPage()
                .addNoteBookToWishlist(note3)
                .addNoteBookToWishlist(note4)
                .addItemToWishlist(note5);
        Thread.sleep(2000);

        JavascriptExecutor js = (JavascriptExecutor) WebDriverHolder.getInstance().getDriver();
        js.executeScript("window.scrollTo(0, 0)");

        new BasePage();
        BasePage
                .getMainMenu()
                .selectWishlistPage();

        List<String> expectedProducts = List.of("HP Spectre XT Pro UltraBook", "Lenovo Thinkpad Carbon Laptop", "Samsung Premium Ultrabook");
        List<String> actualProducts = new WishlistPage().getProductsFromTable()
                .stream()
                .map(ProductModelWishlist::productName)
                .toList();

        new WishlistPage().useCheckboxAddToCart(note1);
        new WishlistPage().clickOnAddToCartButton();

        int expectedCartQtyAfterAddToCart = 4;
        int actualCartQtyAfterAddToCart = new WishlistPage().getWishlistValue();

    }
}


