package org.turko;
import org.turko.pages.base.BasePage;
import org.turko.pages.content.GiftCard.GiftCardsPage;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.turko.pages.content.GiftCard.ProductGiftCardsPage;

import static org.assertj.core.api.Assertions.assertThat;

public class ErrorMessageInGiftCardTest extends BaseTest{
    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void errorMessageInGiftCard() throws InterruptedException {
        new BasePage()
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItem("Gift Cards");

        Thread.sleep(1000);

        new GiftCardsPage().clickProductByName();

        Thread.sleep(1000);

        new ProductGiftCardsPage().clickOnAddToCartButton();

        Thread.sleep(1000);

        assertThat(ProductGiftCardsPage.isErrorMessage())
                .as("Error Message should be visible")
                .isTrue();
        assertThat(new ProductGiftCardsPage().getErrorMessage())
                .as("Error message should be 'Enter valid recipient name'")
                .isEqualTo("Enter valid recipient name");


        ProductGiftCardsPage.enterRecipientName("Test Name");
        ProductGiftCardsPage.enterSenderNameField("");

        int initialCartValue = new BasePage().getShoppingCartValue();
        new ProductGiftCardsPage().clickOnAddToCartButton();

        Thread.sleep(1000);

        assertThat(ProductGiftCardsPage.isErrorMessage())
                .as(" Error Message should be visible")
                .isTrue();
        assertThat(new ProductGiftCardsPage().getErrorMessage())
                .as("Error message should be 'Enter valid sender name'")
                .isEqualTo("Enter valid sender name");

        Thread.sleep(1000);

        ProductGiftCardsPage.enterSenderNameField("Sender Name");
        new ProductGiftCardsPage().clickOnAddToCartButton();

        Thread.sleep(1000);

        int updatedCartValue = new BasePage().getShoppingCartValue();
        assertThat(updatedCartValue)
                .as("Number of products on Shopping cart should be correct")
                .isEqualTo(initialCartValue + 1);
    }

}
