package org.turko;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.turko.driver.WebDriverHolder;
import org.turko.utils.PropertyReader;

public class BaseTest {

    @BeforeSuite
    public void beforeClass() {
        WebDriverHolder.getInstance();
    }

    @AfterSuite(alwaysRun = true)
    public void afterClass() {
        WebDriverHolder.getInstance().quitDriver();
    }
    protected void openUrl(String url) {
        String baseUrl = PropertyReader.getInstance().getProperty("baseUrl");
        if (url.startsWith("/"))
            WebDriverHolder.getInstance().getDriver().get(baseUrl + url);
        else WebDriverHolder.getInstance().getDriver().get(url);
    }

    protected void openUrl() {
        openUrl(PropertyReader.getInstance().getProperty("baseUrl"));
    }
}
