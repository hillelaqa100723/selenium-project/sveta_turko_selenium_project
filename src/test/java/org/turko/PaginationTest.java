package org.turko;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.turko.model.ProductModel;
import org.turko.pages.base.BasePage;
import org.turko.pages.content.computers.NotebooksPage;

import static org.assertj.core.api.Assertions.assertThat;
import static org.testng.Assert.assertNotEquals;

import java.util.List;

public class PaginationTest  extends BaseTest {

    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void PaginationTest() throws InterruptedException {
        openUrl();
        BasePage
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers",
                        "Notebooks");

        new NotebooksPage().selectDisplayPerPage(3);
        Thread.sleep(2000);

        List<ProductModel> notebooksOnCurrentPage = new NotebooksPage().getNotebooks();

        new NotebooksPage().clickOnNextPageButton();
        Thread.sleep(2000);

        assertThat(new NotebooksPage().getHighlightedPageNumber())
                .as("Page 2 should is highlighted")
                .isEqualTo(2);


        List<ProductModel> notebooksOnNextPage = new NotebooksPage().getNotebooks();

        assertThat(notebooksOnCurrentPage)
                .as("List of items is changed")
                .isNotEqualTo(notebooksOnNextPage);

        new NotebooksPage().clickOnPrevPageButton();
        Thread.sleep(2000);

        assertThat(new NotebooksPage().getHighlightedPageNumber())
                .as("Page 1 should is highlighted")
                .isEqualTo(1);

        List<ProductModel> notebooksOnPrevPagePage = new NotebooksPage().getNotebooks();
        Thread.sleep(1000);

        assertThat(notebooksOnPrevPagePage)
                .as("List of items is changed")
                .isNotEqualTo(notebooksOnNextPage);
    }
}
