package org.turko;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.turko.pages.base.BasePage;

import static org.assertj.core.api.Assertions.assertThat;

public class LogoutTest extends BaseTest {

    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void Logout() {
        BasePage basePage = new BasePage()
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser();

        BasePage
                .getMainMenu()
                .selectLogout();

        assertThat(BasePage.getMainMenu().isRegisterMenuItemVisible())
                .as("Register menu item should be visible")
                .isTrue();
        assertThat(BasePage.getMainMenu().isLoginMenuItemVisible())
                .as("Login menu item should be visible")
                .isTrue();

    }
}
