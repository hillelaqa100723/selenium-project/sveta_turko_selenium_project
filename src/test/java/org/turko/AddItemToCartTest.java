package org.turko;

import org.turko.pages.base.BasePage;
import org.turko.pages.content.computers.NotebooksPage;
import org.turko.pages.content.computers.ProductNotebooksPage;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AddItemToCartTest extends BaseTest {
    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void getProducts() throws InterruptedException {
        new BasePage()
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers",
                        "Notebooks");

        new NotebooksPage()
                .clickProductByName("HP Spectre XT Pro UltraBook");

        Thread.sleep(2000);

        assertThat(new ProductNotebooksPage().isPageWithItemLoaded())
                .as("Page with item is loaded.")
                .isTrue();

        int initialCartValue = BasePage.getShoppingCartValue();

        new ProductNotebooksPage().clickOnAddToCartButton();
        Thread.sleep(2000);

        assertThat(new ProductNotebooksPage()
                .isInfoMessageDisplayed())
                .isTrue();

        Thread.sleep(6000);

        int updatedCartValue = new ProductNotebooksPage().getShoppingCartValue();
        assertThat(updatedCartValue)
                .as("Shopping cart value increased by 1.")
                .isEqualTo(initialCartValue + 1);
    }
}

