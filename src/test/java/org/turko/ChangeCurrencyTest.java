package org.turko;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.turko.pages.base.BasePage;
import org.turko.pages.base.Currencies;
import org.turko.pages.content.computers.NotebooksPage;

import static org.assertj.core.api.Assertions.assertThat;

public class ChangeCurrencyTest extends BaseTest {

    @BeforeMethod
    public void beforeMethod() {
        openUrl();
    }

    @Test
    public void changeCurrency() throws InterruptedException {
        new BasePage()
                .getMainMenu()
                .selectLogin()
                .loginWithDefaultUser()
                .getNavigationMenu()
                .selectNavigateMenuItemWithSubItem("Computers",
                        "Notebooks");

        new BasePage().selectCurrency(Currencies.EURO);
        assertThat(NotebooksPage.areActualCurrencyPrice("€"))
                .as("All product prices should be in EURO")
                .isTrue();

        new BasePage().selectCurrency(Currencies.US_DOLLAR);

        assertThat(NotebooksPage.areActualCurrencyPrice("$"))
                .as("All product prices should be in DOLLAR")
                .isTrue();
    }

}
