package org.turko;

import lombok.SneakyThrows;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import org.turko.pages.base.BasePage;
import org.turko.pages.login.LoginPage;
import org.turko.utils.PropertyReader;
import static  org.assertj.core.api.Assertions.assertThat;

public class LoginTest extends BaseTest {

    @BeforeMethod
    public void beforeMethod() {
        openUrl();
        assertThat(new BasePage().isTheMainPage())
                .as("User should be on the main page")
                .isTrue();
    }

    @SneakyThrows
    @Test
    public void loginTest() {

        BasePage
                .getMainMenu();
        assertThat(BasePage.getMainMenu().isRegisterMenuItemVisible())
                .as("Register menu item should be visible")
                .isTrue();
        assertThat(BasePage.getMainMenu().isLoginMenuItemVisible())
                .as("Login menu item should be visible")
                .isTrue();

        BasePage.getMainMenu().selectLogin();

        String userEmail = PropertyReader.getInstance().getProperty("defaultUser");
        String userPass = PropertyReader.getInstance().getProperty("defaultPass");

        LoginPage.login(userEmail, userPass);
            Thread.sleep(2000);

        assertThat(new LoginPage().isTheLoginPageLoaded())
                .as("Login form on the login page is loaded!")
                .isTrue();

            assertThat(BasePage.getMainMenu().isLogOutMenuItemVisible())
                    .as("Logout menu item should be visible")
                    .isTrue();
            assertThat(BasePage.getMainMenu().isMyAccountMenuItemVisible())
                    .as("MyAccount menu item should be visible")
                    .isTrue();
    }
}
